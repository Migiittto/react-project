var express = require('express');
var router = express.Router();
var url = require('url');
const https = require('https');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/search', function(req, res, next) {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  var options = {
    host: 'www.foodie.fi',
    port:443,
    path:'/products/search2?term='+query['term'],
    headers:{
      'X-Requested-With':'XMLHttpRequest'
    }
  }
  https.get(options, (resp) => {
    let data = '';

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk;
    });

    resp.on('end', ()=>{
      res.send(data);
    })
  });
});

module.exports = router;
