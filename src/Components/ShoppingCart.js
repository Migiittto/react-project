import React, { Component } from 'react';

class ShoppingList extends Component{

  getPrice(){
    var total=0.00;
    for (var i = 0; i < this.props.cart.length; i++) {
      total=total+this.props.cart[i].price;
    }
    return total.toFixed(2);
  }

  getButton(item){
    if(!item.checked){
      return <button className='btn btn-danger' onClick={()=>this.props.removeItem(item, true)}><span className='glyphicon glyphicon-remove'></span></button>;
    }
    return <button className='btn btn-success'  onClick={()=>this.props.removeItem(item, false)}><span className='glyphicon glyphicon-ok'></span></button>;
  }

  render(){
    var list = this.props.cart.map(item =>
      <tr key={item.ean}>
        <td>{item.name} ({item.aisle_number})</td>
        <td>{item.price}&euro;</td>
        <td>{this.getButton(item)}</td>
      </tr>)
    return <table>
      <thead><tr><td>Name (Aisle)</td><td>Price</td><td>R</td></tr></thead>
      <tbody>{list}<tr><td style={{textAlign:"end", borderTop:'1px solid black'}} colSpan={3}><h4><b>Total: {this.getPrice()}&euro;</b></h4></td></tr></tbody></table>
  }
}

class ShoppingCart extends Component{
  constructor(props){
    super(props);
    this.saveNotFoundItems = this.saveNotFoundItems.bind(this);
    this.emptySavedCart = this.emptySavedCart.bind(this);
  }

  saveNotFoundItems(){
    let array = [];
    for (var i = 0; i < this.props.cart.length; i++) {
      if(!this.props.cart[i].checked){
        array.push(this.props.cart[i]);
      }
    }
    localStorage.setItem("stinfo_cart", JSON.stringify(array));
  }

  emptySavedCart(){
    localStorage.setItem("stinfo_cart", JSON.stringify([]));
    this.props.resetCart();
  }

  render(){
     return <div className='pageContainer'>
       <div onClick={this.saveNotFoundItems}>
       <h1>Shopping List</h1>
       <br></br>
       <ShoppingList removeItem={this.props.removeItem} cart={this.props.cart} />
       </div>
       <br />
       <button onClick={this.emptySavedCart}>Empty cart</button>
     </div>
  }
}

export default ShoppingCart;
