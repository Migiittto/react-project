import React, { Component } from 'react';

class StoreList extends Component{
    constructor(props)
    {
        super(props);

        this.setDefault = this.setDefault.bind(this);
    }

    names = {}

    setDefault(item) {
      console.log(item);
        this.props.setStore(item.name);
    }

    isOpen(store) {
        if (store.open)
            return <div id="storeOpen" className="open" />
        else
            return <div id="storeOpen"/>
    }

    getOpeningHours(times) {
        return times[new Date().getDay()].split(': ')[1] 
    }

    getTimeLeft(times) {
        console.log(times);
        if (!times.open_now)
            return 'Closed'

        let closingTime = times.periods[new Date().getDay() === 0 ? 6 : new Date().getDay() - 1];
        console.log(closingTime);

        let hours = Math.floor(closingTime.close.time / 100);
        let minutes = closingTime.close.time - (hours * 100)

        let today = new Date(0, 0, closingTime.open.day, new Date().getHours(), new Date().getMinutes());
        let close = new Date(0, 0, closingTime.close.day, hours, minutes);

        let diff = new Date(close - today);
        let text = '';

        if (diff.getUTCHours() > 0)
            text += diff.getUTCHours() + ' hours, '

        if (diff.getUTCMinutes() > 0)
            text += diff.getUTCMinutes() + ' minutes'

        return 'Still open for: ' + text;
    }

    render(){
        console.log(this.props);

        return (
        <div id="storelist">
            {this.props.stores.map((store, key) => {
                this.names[key] = store.name;

                return (
                <div className='storeWrap' key={key}>
                    <div className="storeName">
                        <span>{store.name} </span>
                    </div>
                    
                    <div className='openingTimes'>
                         <span className={store.open ? 'open' : ''}> Open today: {this.getOpeningHours(store.opening_hours.weekday_text)}</span>
                        <span className='storeTimeLeft'>{this.getTimeLeft(store.opening_hours)}</span>
                    </div>

                    <button id='setDefault' className="btn btn-success" storeName={store.name} onClick={()=>this.setDefault(store)}>
                        Select store
                    </button>

                </div>
                )
            })}
        </div>
     )}
}



export default StoreList;
