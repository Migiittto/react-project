import React, { Component } from 'react';
import Map from './Map'

class Stores extends Component{
  render(){
     return <div className='pageContainer'>
       <h1>Store</h1>
       <Map setStore={this.props.setStore}/>
     </div>
  }
}

export default Stores;
