import React, { Component } from 'react';
import StoreList from './StoreList';
import StoreHeaders from './StoreHeaders';
/*
    {
  "formatted_address": "Orioninkatu 2, 53850 Lappeenranta, Finland",
  "geometry": {
    "location": {},
    "viewport": {
      "f": {
        "b": 61.05065951970849,
        "f": 61.0533574802915
      },
      "b": {
        "b": 28.103804569708473,
        "f": 28.106502530291436
      }
    }
  },
  "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png",
  "id": "7ef6b4dcffd190a87cb4e91f6734a96b9f5a8b2a",
  "name": "S-market Sammonlahti",
  "opening_hours": {
    "open_now": true,
    "weekday_text": []
  },
  "photos": [
    {
      "height": 3142,
      "html_attributions": [
        "<a href=\"https://maps.google.com/maps/contrib/110471098607858380500/photos\">Tuomas Vitikainen</a>"
      ],
      "width": 4190
    }
  ],
  "place_id": "ChIJsa0sleSUkEYRPSTyq-ubX0g",
  "rating": 4,
  "reference": "CmRRAAAAdAix06CQzjuMAadTn4p11-6kIvX6WGsQe3FvVe1yvbSrVi-FeirM5YoB6y529RoK7UUCKtBhqDa0RIVly19tXUipp4ThUs3kky0oCUs4B8siurtjdCVJWb-xRhwXmvsVEhAqsIHjsIsZccZr2JFV-F7DGhQ1HVEdMp9b5QSRt0KAhSJUg218jg",
  "types": [
    "supermarket",
    "grocery_or_supermarket",
    "food",
    "store",
    "point_of_interest",
    "establishment"
  ],
  "html_attributions": [],
  "visible": true
}
*/

/*
    Why oh why, won't CORS work
    have to resort to copy/paste

    This is the example code from react-google-maps docs
*/


/*
  Bugs:
    - When having multiple
*/

const _ = require("lodash");
const { compose, withProps, lifecycle } = require("recompose");
const {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} = require("react-google-maps");
const { SearchBox } = require("react-google-maps/lib/components/places/SearchBox");

const stores = [];
const defaultDistance = 3;

function degreesToRadians(degrees) {
  return degrees * Math.PI / 180;
}

function gpsDistance(lat1, lon1, lat2, lon2) {
  var earthRadiusKm = 6371;

  var dLat = degreesToRadians(lat2-lat1);
  var dLon = degreesToRadians(lon2-lon1);

  lat1 = degreesToRadians(lat1);
  lat2 = degreesToRadians(lat2);

  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);

  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  return earthRadiusKm * c;
}

const Map = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyC4R6AN7SmujjPUIGKdyao2Kqitzr1kiRg&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} className='herpity'/>,
    containerElement: <div style={{ height: `400px` }} className='derpity' />,
    mapElement: <div style={{ height: `100%` }} className='bob'/>,
  }),
  lifecycle({
    componentWillMount() {
      const refs = {}

      this.setState({
        bounds: null,
        center: {
            lat: 61.063688, lng: 28.092993
        },
        markers: [],
        lastUpdate: 0,
        mounted: false,
        onMapMounted: ref => {
          refs.map = ref;

          while (stores.length !== 0)
            stores.pop();

          this.setState({mounted: true})
        },
        onBoundsChanged: () => {
          this.setState({
            bounds: refs.map.getBounds(),
            center: refs.map.getCenter(),
          })
        },
        onSearchBoxMounted: ref => {
          refs.searchBox = ref;

          console.log("Loaded");

        },
        onMarkerClick: () => {
          //show a info box about the store (opening hours and such)
          //
        },
        onPlacesChanged: () => {
          while (stores.length !== 0)
            stores.pop();

          const places = refs.searchBox.getPlaces();
          const bounds = new window.google.maps.LatLngBounds();

          var googleMapsClient = require('@google/maps').createClient({
            key: 'AIzaSyCwakHDuwCKS731AgWpn2vX3Gpv14RZP9Q'
          });

          let newStores = [];

          places.forEach(place => {
            if (place.geometry.viewport) {
              bounds.union(place.geometry.viewport)
            } else {
              bounds.extend(place.geometry.location)
            }

            let lng = place.geometry.location.lng();
            let lat = place.geometry.location.lat();

            let dist = gpsDistance(this.state.center.lat(), this.state.center.lng(), place.geometry.location.lat(), place.geometry.location.lng());
            console.log(dist);
            place.visible = dist < defaultDistance;

            if (!place.visible)
              return;

              googleMapsClient.place({placeid: place.place_id}, (err, response) => {
                if (err)
                  return;

                let data = response.json.result;
                const store = {};

                store.name = data.name;
                store.opening_hours = data.opening_hours;
                store.address = data.formatted_address;
                store.open = data.opening_hours.open_now;
                store.rating = data.rating;

                stores.push(store);

                this.setState({lastUpdate: new Date().getTime()});
              })
          });

          const nextMarkers = places.map(place => ({
            position: place.geometry.location,
            visible:  place.visible
          }));
          const nextCenter = _.get(nextMarkers, '0.position', this.state.center);

          this.setState({
            center: nextCenter,
            markers: nextMarkers,
          });

        },
      })
    },
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <div>
  <GoogleMap
    ref={props.onMapMounted}
    defaultZoom={15}
    center={props.center}
    onBoundsChanged={props.onBoundsChanged}
    onDrag={props.refresh}
  >
    <SearchBox
      ref={props.onSearchBoxMounted}
      bounds={props.bounds}
      controlPosition={window.google.maps.ControlPosition.TOP_LEFT}
      onPlacesChanged={props.onPlacesChanged}
      onChange={props.test}
    >
      <input
        type="text"
        onLoad={props.test}
        value={props.value}
        placeholder="Customized your placeholder"
        onChange={props.test}
        style={{
          boxSizing: `border-box`,
          border: `1px solid transparent`,
          width: `240px`,
          height: `32px`,
          marginTop: `27px`,
          padding: `0 12px`,
          borderRadius: `3px`,
          boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
          fontSize: `14px`,
          outline: `none`,
          textOverflow: `ellipses`,
        }}
      />
    </SearchBox>
    {props.markers.map((marker, index) =>
      <Marker key={index} position={marker.position} visible={marker.visible}/>
    )}
  </GoogleMap>
  <StoreHeaders headers={{name: 'Store name', hours: 'Opening hours (today)', default: 'Set as main store'}}/>
  <StoreList setStore={props.setStore} stores={stores}/>
  </div>
);

<Map />
export default Map;
