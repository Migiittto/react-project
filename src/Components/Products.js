import React, { Component } from 'react';
import * as $ from 'jquery';

class ProductList extends Component{
  getImgUrl(ean){
    return 'https://foodieimages.s3.amazonaws.com/images/entries/180x220/'+ean+'_0.png';
  }

  render(){
    if(this.props.prodlist.length ===0){
      return null;
    }
    var elems = this.props.prodlist.map(item => <li key={item.ean} className='productContainer col-xs-12'>
      <div className='col-xs-3'><img className='productImage' alt='Not-found' src={this.getImgUrl(item.ean)} /> </div>
      <div className='col-xs-9' style={{marginTop:50,marginBottom:30}}>
        <span className='productText'>{item.name} ({item.aisle}, Aisle {item.aisle_number}) <h2> {item.price}&euro;</h2></span>
        <br />
        <button onClick={()=>this.props.addToCart(item)} className='productButton btn btn-success btn-lg'>Add To List</button>
      </div>
    </li>)
    return <div style={{maxHeight:750, overflowY:"scroll"}}><ul>{elems}</ul></div>;
  }
}

class Products extends Component{
  constructor(props){
    super(props);
    this.state={search:'', products:[]};
    this.updateSearch = this.updateSearch.bind(this);
    this.searchKeyPress = this.searchKeyPress.bind(this);
    this.getProducts = this.getProducts.bind(this);
  }

  getProducts(term){
    //jQuery binds this, we need to change reacts this to another variable, this2;
    let this2 = this;
    var productslist = [];
    $.ajax({
      method:'GET',
      accepts:{
        'json':'application/json, text/javascript',
      },
      url:"http://localhost:3000/search?term="+term,
      dataType:"json"
    }).then(function(res){
      productslist=res.data.entries;
      this2.setState({products:productslist});
    });
  }

  updateSearch(evt){
    this.setState({search:evt.target.value.replace(" ", "%20")})
  }

  searchKeyPress(evt){
    if(evt.key==='Enter'){
      this.getProducts(this.state.search);
    }
  }

  render(){
     return <div className='pageContainer'>
       <h1>Products</h1>
        <div className='input-group visible-xs'>
          <input className='form-control visible-xs' placeholder='Search products here' type='text' onKeyPress={this.searchKeyPress} onChange={this.updateSearch}/>
          <div className='input-group-addon' onClick={()=>this.searchKeyPress({key:'Enter'})}><span className='glyphicon glyphicon-search'></span></div>
        </div>
        <div className='input-group hidden-xs' style={{width:300, margin:"auto"}}>
          <input className='form-control hidden-xs' placeholder='Search products here' type='text' onKeyPress={this.searchKeyPress} onChange={this.updateSearch}/>
          <div className='input-group-addon' onClick={()=>this.searchKeyPress({key:'Enter'})}><span className='glyphicon glyphicon-search'></span></div>
        </div>
        <br/>
        <ProductList prodlist={this.state.products} addToCart={this.props.addToCart}/>
     </div>
  }
}

export default Products;
