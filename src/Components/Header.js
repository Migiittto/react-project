import React, { Component } from 'react';
import logo from '../Stinfo-logo.png';
import logoxs from '../Stinfo-logo-xs.png';
import Menu, {Item as MenuItem } from 'rc-menu';

class Header extends Component{
  render(){
     return <header className="App-header">
      <div className='col-sm-5 hidden-xs'>
        <img onClick={() => this.props.setView({key:0})} src={logo} className="App-logo" alt="logo" />
        <div id="defaultStoreDiv">Store: {this.props.store} </div>
      </div>
      <div className='col-xs-9 visible-xs'>
        <img onClick={() => this.props.setView({key:0})} src={logoxs} className="App-logo" alt="logo" />
      </div>
      <div className='col-sm-7 hidden-xs' style={{ marginTop: 30}}>
        <Menu onSelect={(obj) => this.props.setView(obj)} mode='horizontal'>

          <MenuItem key='1'>Stores</MenuItem>
          <MenuItem key='2'>Products</MenuItem>
          <MenuItem key='3'>Shopping list</MenuItem>
          <MenuItem key='4'>About</MenuItem>
        </Menu>
      </div>
      <MenuXs setView={this.props.setView}/>
    </header>
  }
}

class MenuXs extends Component{
  constructor(props){
    super(props);
    this.toggleMenu = this.toggleMenu.bind(this);
    this.state = {menu:false};
  }

  toggleMenu(){
    this.setState({menu:!this.state.menu});
  }

  render(){
    if(this.state.menu){
      return <div className='col-xs-3 visible-xs' style={{ marginTop: 30}}>
      <button  onClick={this.toggleMenu} className='menuButton'><span className='glyphicon glyphicon-option-vertical'></span></button>
      <Menu onSelect={(obj) => this.props.setView(obj)} className='xs-menu-position' mode='inline'>
      <MenuItem key='1'>Stores</MenuItem>
      <MenuItem key='2'>Products</MenuItem>
      <MenuItem key='3'>Shopping list</MenuItem>
      <MenuItem key='4'>About</MenuItem>
      </Menu>
      </div>
    }
    return <div className='col-xs-3 visible-xs' style={{ marginTop: 30}}>
    <button onClick={this.toggleMenu} className='menuButton'><span className='glyphicon glyphicon-option-vertical'></span></button>
    </div>
  }
}

export default Header;
