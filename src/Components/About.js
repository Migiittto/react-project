import React, { Component } from 'react';

class About extends Component{
  render(){
     return <div className='pageContainer'>
       <h1>About</h1>
	       <h5>  Stinfo is the group project created by Mikael Sommarberg, Miikka Lahtinen and Julia Tasa in ReactJS Code Camp in January 2018. 
	       <br></br>
	       Stinfo is a grocery store info for all citizens but especially for students, who want to see the stores nearby and stores' opening hours. 
	       <br></br>
	       Also, Stinfo makes it possible to check the prices of the products! 
	       <br></br> 
	       <b>Mikael Sommarberg</b> has experience in build management and software integration. He has been developing, for example,  with jQuery, AngularJS and Python. His role in this project is to integrating and reactifying. <br></br>
	       <b>Miikka Lahtinen </b> is mostly self-taught programmer and very interested in programming languages. He is responsible for API-queries and open data functionality. <br></br>
	       <b>Julia Tasa </b> is interested in user-oriented design, and she works as a designer in this project. 

	       </h5> 
	     </div>
  }
}

export default About;
