import React, { Component } from 'react';

class Home extends Component{
  render(){
     return <div className='pageContainer'>
       <h1>Welcome to Stinfo!</h1>
       <h3>Stinfo aims to help you do everyday grocery shopping easier!<br></br> Now you can find the stores near you easily. <br></br> <b>Have good time with shopping!</b></h3>
     </div>
  }
}

export default Home;
