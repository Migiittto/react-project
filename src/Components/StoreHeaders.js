import React, {Component} from 'react';

class StoreHeaders extends Component {
    render() {
        return  (
        <div id="storeHeaders">
            <div className="storeHeader" id="headerName">
                {this.props.headers.name}
            </div>
            <div className="storeHeader" id="headerHours">
                {this.props.headers.hours}
            </div>
            <div className="storeHeader" id="headerDefault">
                {this.props.headers.default}
            </div>
        </div>
        )
    }
}

export default StoreHeaders;
