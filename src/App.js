import React, { Component } from 'react';
import './App.css';
import 'rc-menu/assets/index.css';
import Header from './Components/Header.js';
import Home from './Components/Home.js';
import Products from './Components/Products.js';
import About from './Components/About.js';
import ShoppingCart from './Components/ShoppingCart.js';
import Stores from './Components/Stores.js';


class App extends Component {
  constructor(props){
    super(props);

    let cart = [];
    try {
      if(localStorage.getItem("stinfo_cart")){
        cart = JSON.parse(localStorage.getItem("stinfo_cart"));
      }
    } catch (e) {
      cart=[];
    } finally {
      if(typeof(cart)!=='object'){
        cart=[];
      }
    }

    this.state = {view:'0', cart:cart, store:'not selected'};
    this.setView = this.setView.bind(this);
    this.addToCart = this.addToCart.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.setStore = this.setStore.bind(this);
    this.resetCart = this.resetCart.bind(this);
  }

  setView(target){
    console.log('setting view', target.key);
    this.setState({view:target.key});
  }

  resetCart(){
    this.setState({cart:[]})
  }

  removeItem(item, status){
    let cart = this.state.cart;
    for (var i = 0; i < cart.length; i++) {
      if (cart[i].ean ===item.ean){
        cart[i].checked = status;
      }
    }
    this.setState({cart:cart});
  }

  addToCart(item){
    if(!this.state.cart){
      this.setState({cart:[]})
    }
    let cart = this.state.cart;
    item.checked=false;
    cart.push(item);
    this.setState({cart:cart});
    let array = [];
    for (var i = 0; i < cart.length; i++) {
      if(!cart[i].checked){
        array.push(cart[i]);
      }
    }
    localStorage.setItem("stinfo_cart", JSON.stringify(array));
  }

  setStore(name){
    this.setState({store:name});
  }

  getView(){
    switch(this.state.view){
      case '0':
        return <Home />

      case '1':
        return <Stores setStore={this.setStore}/>

      case '2':
        return <Products addToCart={this.addToCart}/>

      case '3':
        return <ShoppingCart resetCart={this.resetCart} removeItem={this.removeItem} cart={this.state.cart}/>

      case '4':
        return <About />

      default:
        return <Home />
    }
  }

  render() {
    return (
      <div className="App">
        <Header setView={this.setView} store={this.state.store}/>
        {this.getView()}
      </div>
    );
  }
}

export default App;
