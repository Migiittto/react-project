# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This reposity is for the LUT Course project for Code Camp React. (codecamp.fi for more info).
Our group consisted of 3 persons, Mikael Sommarberg, Miikka Lahtinen and Julia Tasa.

### How do I get set up? ###

clone the repository and install Node 8.9.4 with npm.
navigate to the project root directory and run command "npm install"
navigate to the project root directory, and then to "backend" directory, and run command "npm install"
When npm has finished installing requirements start 2 terminals and navigate to project root directory with both

first terminal:
ROOT\ npm start

second terminal:
ROOT\backend npm start

Now you should be able to go to http://localhost:3000/ and see Stinfo homepage.


### Creators ###
Mikael Sommarberg (mikael.sommarberg@student.lut.fi)
Miikka Lahtinen (miikka.lahtinen@student.lut.fi)
Julia Tasa (julia.tasa@student.lut.fi)